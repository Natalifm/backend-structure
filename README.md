### Project structure
![Screenshot](reademeImg/structure.png)

* **_Dependencies_**

![Screenshot](reademeImg/dependencies.png)

The architecture of our application is based on a typical MVC model. Our Client-side will be written in Javascript, HTML, and CSS, using ReactJS as the framework. This level of the architecture is what the user will interact with to access the features of our application.

A server-side (controller) will be written using NodeJS and Express and will act as a bridge of communication for the Client-side and Database. 
This tier will serve HTML pages to the user’s device and accept HTTP requests from
 the user and follow with the appropriate response. We will be using the HTTP 
 protocol to receive HTTP requests from the user and follow the appropriate response. 
 Since we do not use a microservice architecture, it is not rational to connect rabbitMQ or WebSocket,
 which will be more appropriate for chat applications or multiplayer games.

Our Database will be hosting MongoDB. This is where we will store all of the crucial data our application needs to function.

## 

* For this MVP system, in my opinion, it is more efficient and better to choose a monolithic architecture, because it will allow us to launch the product faster.

  When assembled correctly, monolithic applications tend to perform better, all pieces of software are unified, and all of its functionality is managed in one place. The components of monolithic software are interconnected and interdependent, which helps the software to be self-contained. Unlike a microservice-based application. For example, an application with a microservices architecture might need to make 40 API calls to 40 different microservices to load each screen, which obviously leads to poor performance. Monolithic applications in turn enable faster communication between software components through shared code and memory.
  
  Microservices for this task, in my opinion, is not the best option, as this architecture is suitable for more complex applications that need to be divided into separate services, takes much more time and money, and will require more developers.
  
