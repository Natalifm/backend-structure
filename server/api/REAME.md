Api (controllers) respond to various http requests.
* routes - containing all route files describing all route paths for application
* middleware - containing all middleware for the routes (authenticationMiddleware, authorizationMiddleware, errorHandlerMiddleware, jwtMiddleware ect.)
* services - additional data processing, or take the necessary data from the database

